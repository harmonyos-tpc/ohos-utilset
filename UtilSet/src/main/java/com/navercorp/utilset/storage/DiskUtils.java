package com.navercorp.utilset.storage;

import ohos.app.Context;

import ohos.data.usage.DataUsage;
import ohos.data.usage.MountState;

/**
 * <b>Cautious!</b><br>
 * External disk do not necessarily mean that it is a SD Card.<br>
 *
 * Any large size of space can be external disk.<br>
 * As such, do not make assumptions that you are working on SDCard.<br>
 *
 * @author jaemin.woo
 */
public final class DiskUtils {
    /**
     * TEMPORARY_FOLDER
     */
    protected static final String TEMPORARY_FOLDER = "/temp";

    /**
     * CACHE_FOLDER
     */
    protected static final String CACHE_FOLDER = "/cache";

    private DiskUtils() {
        /* Do nothing */
    }

    /**
     * isExternalStorageMounted
     * Checks if External storage is mounted
     * @return true if mounted; false otherwise
     */
    public static boolean isExternalStorageMounted() {
        return DataUsage.getDiskMountedStatus().equals(MountState.DISK_MOUNTED);
    }

    /**
     * Returns directory name to save cache data in the external storage<p>
     * This method always returns path of external storage even if it does not exist.<br>
     * As such, make sure to call isExternalStorageMounted method as state-testing method and
     * then call this function only if state-testing method returns true.
     *
     * @param context Context to get external storage information
     * @return String containing cache directory name
     */
    public static String getExternalDirPath(Context context) {
        return context.getExternalCacheDir().getAbsolutePath();
    }

    /**
     * Returns directory name to save temporary files in the external storage for temporary<p>
     * This method always returns path of external storage even if it does not exist.<br>
     * As such, make sure to call isExternalStorageMounted method as state-testing method and
     * then call this function only if state-testing method returns true.
     *
     * @param context Context to get external storage information
     * @return String containing temporary directory name
     */
    public static String getExternalTemporaryDirPath(Context context) {
        return context.getExternalFilesDir(null).getAbsolutePath();
    }

    /**
     * Returns root directory of the external storage.<p>
     * This method always returns path of external storage even if it does not exist.<br>
     * As such, make sure to call isExternalStorageMounted method as state-testing method and
     * then call this function only if state-testing method returns true.
     *
     * @param context Context to get external storage information
     * @return String containing external root directory name
     */
    public static String getExternalContextRootDir(Context context) {
        return context.getExternalFilesDir(null).getAbsolutePath();
    }
}