package com.navercorp.utilset.audio;

import com.navercorp.utilset.utils.UtilSetLogUtils;

import ohos.media.audio.AudioManager;
import ohos.media.audio.AudioRemoteException;

/**
 * Controls Media Volume
 *
 * @author jaemin.woo
 *
 */
public class VolumeUtils {
    private static final String TAG = VolumeUtils.class.getSimpleName();

    private static AudioManager mAudioManager;

    private VolumeUtils() {
        /* Do nothing */
    }

    /**
     * Returns current media volume
     *
     * @return current volume
     */
    public static int getCurrentVolume() {
        mAudioManager = new AudioManager();
        int volume = 0;
        try {
            volume = mAudioManager.getVolume(AudioManager.AudioVolumeType.STREAM_MUSIC);
        } catch (AudioRemoteException e) {
            UtilSetLogUtils.error(TAG, "AudioRemoteException");
        }
        return volume;
    }

    /**
     * Sets media volume.<br>
     * When setting the value of parameter 'volume' greater than the maximum value of the media volume
     * will not either cause error or throw exception but maximize the media volume.<br>
     * Setting the value of volume lower than 0 will minimize the media volume.
     *
     * @param volume volume to be changed
     */
    public static void setVolume(int volume) {
        adjustMediaVolume(volume);
    }

    /**
     * Sets media volume and displays volume level.<br>
     * When setting the value of parameter 'volume' greater than the maximum value of the media volume
     * will not either cause error or throw exception but maximize the media volume.<br>
     * Setting the value of volume lower than 0 will minimize the media volume.
     *
     * @param volume volume to be changed
     */
    public static void setVolumeWithLevel(int volume) {
        adjustMediaVolume(volume);
    }

    /**
     * Increases media volume
     */
    public static void increaseVolume() {
        adjustMediaVolume(getCurrentVolume() + 1);
    }

    /**
     * Increases media volume and displays volume level
     */
    public static void increaseVolumeWithLevel() {
        adjustMediaVolume(getCurrentVolume() + 1);
    }

    /**
     * Decreases media volume
     */
    public static void decreaseVolume() {
        adjustMediaVolume(getCurrentVolume() - 1);
    }

    /**
     * Decreases media volume and displays volume level
     */
    public static void decreaseVolumeWithLevel() {
        adjustMediaVolume(getCurrentVolume() - 1);
    }

    /**
     * Returns maximum volume the media volume can have
     *
     * @return Maximum volume
     */
    private static int getMaximumVolume() {
        int maxVolume = 0;
        try {
            maxVolume = mAudioManager.getMaxVolume(AudioManager.AudioVolumeType.STREAM_MUSIC);
        } catch (AudioRemoteException e) {
            UtilSetLogUtils.error(TAG, "AudioRemoteException");
        }
        return maxVolume;
    }

    /**
     * Returns minimum volume the media volume can have
     *
     * @return Minimum volume
     */
    public static int getMinimumVolume() {
        return 0;
    }

    private static void adjustMediaVolume(int volume) {
        final int maximumVolume = getMaximumVolume();
        final int minimumVolume = 0;

        if (volume < minimumVolume) {
            volume = minimumVolume;
        } else if (volume > maximumVolume) {
            volume = maximumVolume;
        }

        AudioManager audioManager = new AudioManager();
        audioManager.setVolume(AudioManager.AudioVolumeType.STREAM_MUSIC, volume);
    }
}

