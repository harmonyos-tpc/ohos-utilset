package com.navercorp.utilset.device;

import ohos.app.Context;

/**
 * Function description
 * class LauncherInfo
 */
public class LauncherInfo {
    private LauncherInfo() {
        /* Do nothing */
    }

    /**
     * getName
     *
     * @param context Context to provide package information
     * @return LauncherInfo
     */
    public static String getName(Context context) {
        return context.getBundleName() + context.getLocalClassName();
    }
}
