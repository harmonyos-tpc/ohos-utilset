package com.navercorp.utilset.cipher;

/**
 * @author jaemin.woo
 */
class CipherObjectFactory {
    private CipherObjectFactory() {
        /* Do nothing */
    }

    /**
     * getInstance
     *
     * @param mode CipherMode
     * @return CipherObject
     */
    public static CipherObject getInstance(CipherMode mode) {
        if (mode == CipherMode.AES) {
            return new AesCipher();
        }
        return new AesCipher();
    }
}
