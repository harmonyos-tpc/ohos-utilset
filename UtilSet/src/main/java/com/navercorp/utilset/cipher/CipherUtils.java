package com.navercorp.utilset.cipher;

/**
 * Provides basic encryption and decryption methods<br>
 * Default cipher algorithm is AES and currently algorithms other than AES are not provided
 *
 * @author jaemin.woo
 */
public class CipherUtils {
    private CipherObject cipherObject;

    /**
     * constructor CipherUtils
     */
    public CipherUtils() {
        this(CipherMode.AES);
    }

    /**
     * constructor CipherUtils
     *
     * @param cipherMode CipherMode
     */
    public CipherUtils(CipherMode cipherMode) {
        cipherObject = CipherObjectFactory.getInstance(cipherMode);
    }

    /**
     * encrypt the text
     *
     * @param seed Seed string which is used for encryption and decryption
     * @param plainText String to be encrypted
     */
    public void encrypt(String seed, String plainText) {
        cipherObject.encrypt(seed, plainText);
    }

    /**
     * decrypt the encrypted value
     *
     * @param seed Seed string which is used for encryption and decryption
     * @param cipherText String encrypted by encrypt method
     */
    public void decrypt(String seed, String cipherText) {
        cipherObject.decrypt(seed, cipherText);
    }
}
