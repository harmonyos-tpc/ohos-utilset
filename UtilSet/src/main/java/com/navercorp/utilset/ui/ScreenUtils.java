package com.navercorp.utilset.ui;

import static ohos.sysappcomponents.settings.SystemSettings.Display.SCREEN_OFF_TIMEOUT;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.DataAbilityHelper;
import ohos.agp.window.service.WindowManager;
import ohos.sysappcomponents.settings.SystemSettings;

/**
 *
 * @author jaemin.woo
 *
 */
public class ScreenUtils {
    private ScreenUtils() {
        /* Do nothing */
    }

    /**
     * setScreenBrightness
     *
     * @param ability Ability
     * @param dim 0.004f is the least brightness to work. if it is set below 0.004f, it won't work.
     */
    public static void setScreenBrightness(Ability ability, float dim) {
        WindowManager.LayoutConfig lp = ability.getWindow().getLayoutConfig().get();
        lp.windowBrightness = dim;
        ability.getWindow().setLayoutConfig(lp);
    }

    /**
     * setScreenOffTimeout
     *
     * @param dataAbilityHelper DataAbilityHelper
     * @param millis Time for screen to turn off. Setting this value to -1 will prohibit screen from turning off
     */
    public static void setScreenOffTimeout(DataAbilityHelper dataAbilityHelper, int millis) {
        SystemSettings.setValue(dataAbilityHelper, SCREEN_OFF_TIMEOUT, String.valueOf(millis));
    }

    /**
     * Prevents screen from being turned off.
     *
     * @param ability Ability
     */
    public static void setScreenOn(Ability ability) {
        ability.getWindow().addFlags(WindowManager.LayoutConfig.MARK_SCREEN_ON_ALWAYS);
    }

    /**
     * Let screen to be turned off
     *
     * @param ability Ability
     */
    public static void clearScreenOn(Ability ability) {
        ability.getWindow().clearFlags(WindowManager.LayoutConfig.MARK_SCREEN_ON_ALWAYS);
    }
}
