/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.navercorp.utilset.sample.slice;

import com.navercorp.utilset.sample.MainAbility;
import com.navercorp.utilset.sample.ResourceTable;
import com.navercorp.utilset.sample.UiAbility;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;

import ohos.agp.components.Component;
import ohos.agp.components.Text;

/**
 * Function description
 * class MainAbilitySlice
 */
public class MainAbilitySlice extends AbilitySlice {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_main_ability_slice);
        initializeViews(intent);
    }

    private void initializeViews(Intent intent) {
        Component textComponent = findComponentById(ResourceTable.Id_cipher_utils);
        if (textComponent instanceof Text) {
            Text cipherUtils = (Text) textComponent;
            cipherUtils.setClickedListener(component -> viewCipherUtils(intent));
        }

        textComponent = findComponentById(ResourceTable.Id_ability_utils);
        if (textComponent instanceof Text) {
            Text abilityUtils = (Text) textComponent;
            abilityUtils.setClickedListener(component -> viewAbilityUtils());
        }

        textComponent = findComponentById(ResourceTable.Id_network_state_utils);
        if (textComponent instanceof Text) {
            Text networkStateUtils = (Text) textComponent;
            networkStateUtils.setClickedListener(component -> viewNetworkStateUtils(intent));
        }

        textComponent = findComponentById(ResourceTable.Id_network_monitor_utils);
        if (textComponent instanceof Text) {
            Text networkMonitorUtils = (Text) textComponent;
            networkMonitorUtils.setClickedListener(component -> viewNetworkMonitorUtils(intent));
        }

        textComponent = findComponentById(ResourceTable.Id_volume_utils);
        if (textComponent instanceof Text) {
            Text volumeUtils = (Text) textComponent;
            volumeUtils.setClickedListener(component -> viewVolumeUtils(intent));
        }

        textComponent = findComponentById(ResourceTable.Id_disk_utils);
        if (textComponent instanceof Text) {
            Text diskUtils = (Text) textComponent;
            diskUtils.setClickedListener(component -> viewDiskUtils(intent));
        }

        textComponent = findComponentById(ResourceTable.Id_compress_utils);
        if (textComponent instanceof Text) {
            Text compressUtils = (Text) textComponent;
            compressUtils.setClickedListener(component -> viewCompressUtils(intent));
        }

        textComponent = findComponentById(ResourceTable.Id_system_utils);
        if (textComponent instanceof Text) {
            Text systemUtils = (Text) textComponent;
            systemUtils.setClickedListener(component -> viewSystemUtils(intent));
        }

        textComponent = findComponentById(ResourceTable.Id_device_utils);
        if (textComponent instanceof Text) {
            Text deviceUtils = (Text) textComponent;
            deviceUtils.setClickedListener(component -> viewDeviceUtils(intent));
        }
    }

    private void viewCipherUtils(Intent intent) {
        Operation cipherOperation = new Intent.OperationBuilder().withAction("action.CipherUtilsSlice")
            .withBundleName(getBundleName())
            .withAbilityName(MainAbility.class.getSimpleName())
            .build();
        intent.setOperation(cipherOperation);
        startAbility(intent);
    }

    private void viewAbilityUtils() {
        Operation operation = new Intent.OperationBuilder().withBundleName(getBundleName())
            .withAbilityName(UiAbility.class.getSimpleName())
            .build();
        Intent intent = new Intent();
        intent.setOperation(operation);
        startAbility(intent);
    }

    private void viewNetworkStateUtils(Intent intent) {
        Operation systemOperation = new Intent.OperationBuilder().withAction("action.NetworkStateListenerUtilsSlice")
            .withBundleName(getBundleName())
            .withAbilityName(MainAbility.class.getSimpleName())
            .build();
        intent.setOperation(systemOperation);
        startAbility(intent);
    }

    private void viewNetworkMonitorUtils(Intent intent) {
        Operation systemOperation = new Intent.OperationBuilder().withAction("action.NetworkMonitorUtilsSlice")
            .withBundleName(getBundleName())
            .withAbilityName(MainAbility.class.getSimpleName())
            .build();
        intent.setOperation(systemOperation);
        startAbility(intent);
    }

    private void viewVolumeUtils(Intent intent) {
        Operation volumeOperation = new Intent.OperationBuilder().withAction("action.VolumeUtilsSlice")
            .withBundleName(getBundleName())
            .withAbilityName(MainAbility.class.getSimpleName())
            .build();
        intent.setOperation(volumeOperation);
        startAbility(intent);
    }

    private void viewDiskUtils(Intent intent) {
        Operation systemOperation = new Intent.OperationBuilder().withAction("action.DiskUtilsSlice")
            .withBundleName(getBundleName())
            .withAbilityName(MainAbility.class.getSimpleName())
            .build();
        intent.setOperation(systemOperation);
        startAbility(intent);
    }

    private void viewCompressUtils(Intent intent) {
        Operation systemOperation = new Intent.OperationBuilder().withAction("action.CompressUtilsSlice")
            .withBundleName(getBundleName())
            .withAbilityName(MainAbility.class.getSimpleName())
            .build();
        intent.setOperation(systemOperation);
        startAbility(intent);
    }

    private void viewSystemUtils(Intent intent) {
        Operation systemOperation = new Intent.OperationBuilder().withAction("action.SystemUtilsSlice")
            .withBundleName(getBundleName())
            .withAbilityName(MainAbility.class.getSimpleName())
            .build();
        intent.setOperation(systemOperation);
        startAbility(intent);
    }

    private void viewDeviceUtils(Intent intent) {
        Operation deviceOperation = new Intent.OperationBuilder().withAction("action.DeviceUtilsSlice")
            .withBundleName(getBundleName())
            .withAbilityName(MainAbility.class.getSimpleName())
            .build();
        intent.setOperation(deviceOperation);
        startAbility(intent);
    }
}
