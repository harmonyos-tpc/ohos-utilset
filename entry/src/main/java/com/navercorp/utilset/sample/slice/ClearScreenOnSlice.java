/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.navercorp.utilset.sample.slice;

import com.navercorp.utilset.sample.ResourceTable;
import com.navercorp.utilset.ui.AbilityUtils;
import com.navercorp.utilset.ui.ScreenUtils;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;

import ohos.agp.components.Text;

/**
 * Function description
 * class ClearScreenOnSlice
 */
public class ClearScreenOnSlice extends AbilitySlice {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ui_utils_ability_slice);
        initializeViews();
    }

    private void initializeViews() {
        ScreenUtils.clearScreenOn(getAbility());

        boolean isPackageInstalled = AbilityUtils.isPackageInstalled(getContext(), getBundleName());

        Text packageInstalled = (Text) findComponentById(ResourceTable.Id_package_installed);
        packageInstalled.setText(String.valueOf(isPackageInstalled));

        Text baseAbilityPackageName = (Text) findComponentById(ResourceTable.Id_base_ability_package_name);
        baseAbilityPackageName.setText(AbilityUtils.getBaseAbilityPackageName(getContext()));

        Text baseAbilityClassName = (Text) findComponentById(ResourceTable.Id_base_ability_class_name);
        baseAbilityClassName.setText(AbilityUtils.getBaseAbilityClassName(getContext()));

        Text topAbilityPackageName = (Text) findComponentById(ResourceTable.Id_top_ability_package_name);
        topAbilityPackageName.setText(AbilityUtils.getTopAbilityPackageName(getContext()));

        Text topAbilityClassName = (Text) findComponentById(ResourceTable.Id_top_ability_class_name);
        topAbilityClassName.setText(AbilityUtils.getTopAbilityClassName(getContext()));

        Text isTopApplicationThis = (Text) findComponentById(ResourceTable.Id_is_top_application_this);
        isTopApplicationThis.setText(String.valueOf(AbilityUtils.isTopApplication(getContext())));

        Text isContextForegroundThis = (Text) findComponentById(ResourceTable.Id_is_context_foreground_this);
        isContextForegroundThis.setText(String.valueOf(AbilityUtils.isContextForeground(getContext())));

        Text isTopApplication = (Text) findComponentById(ResourceTable.Id_is_top_application);
        isTopApplication.setText(String.valueOf(AbilityUtils.isTopApplication(getAbility())));

        Text isContextForeground = (Text) findComponentById(ResourceTable.Id_is_context_foreground);
        isContextForeground.setText(String.valueOf(AbilityUtils.isContextForeground(getAbility())));
    }
}
