/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.navercorp.utilset.sample.slice;

import com.navercorp.utilset.network.NetworkMonitor;

import com.navercorp.utilset.sample.ResourceTable;
import com.navercorp.utilset.sample.util.LogUtil;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;

import ohos.agp.components.Text;

import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;

import java.io.IOException;

/**
 * Function description
 * class NetworkStateListenerUtilsSlice
 */
public class NetworkStateListenerUtilsSlice extends AbilitySlice {
    private static final String TAG = NetworkMonitorUtilsSlice.class.getSimpleName();

    private final NetworkMonitor.NetworkStateChangedListener networkStateChangedListener = this::updateWifiStatus;

    private Text wifiStatus;

    private NetworkMonitor networkUtils;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_network_state_utils_ability_slice);

        networkUtils = NetworkMonitor.getInstance(this);
        networkUtils.addWifiStateChangedListener(networkStateChangedListener);
        initializeViews();
    }

    private void initializeViews() {
        wifiStatus = (Text) findComponentById(ResourceTable.Id_wifi_status_text);

        updateWifiStatus();
    }

    private void updateWifiStatus() {
        boolean isWifiEnabled = networkUtils.isWifiEnabled();
        if (isWifiEnabled) {
            setMessage(ResourceTable.String_wifi_on);
        } else {
            setMessage(ResourceTable.String_wifi_off);
        }
    }

    private void setMessage(int resourceId) {
        String wifiState = null;
        try {
            wifiState = getResourceManager().getElement(resourceId).getString();
        } catch (IOException | NotExistException | WrongTypeException e) {
            LogUtil.info(TAG, "Exception");
        }
        wifiStatus.setText(wifiState);
    }
}
