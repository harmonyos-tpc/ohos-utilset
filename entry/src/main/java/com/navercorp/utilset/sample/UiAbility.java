/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.navercorp.utilset.sample;

import com.navercorp.utilset.sample.slice.AbilityUtilsSlice;
import com.navercorp.utilset.sample.slice.ClearScreenOnSlice;
import com.navercorp.utilset.sample.slice.KeepScreenOnSlice;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

/**
 * Function description
 * class UIAbility
 */
public class UiAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(AbilityUtilsSlice.class.getName());

        addActionRoute("action.AbilityUtilsSlice", AbilityUtilsSlice.class.getName());
        addActionRoute("action.KeepScreenOnSlice", KeepScreenOnSlice.class.getName());
        addActionRoute("action.ClearScreenOnSlice", ClearScreenOnSlice.class.getName());
    }
}
