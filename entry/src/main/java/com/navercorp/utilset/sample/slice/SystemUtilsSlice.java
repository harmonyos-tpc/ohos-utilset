/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.navercorp.utilset.sample.slice;

import com.navercorp.utilset.sample.ResourceTable;
import com.navercorp.utilset.system.SystemUtils;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;

import ohos.agp.components.Text;

/**
 * Function description
 * class SystemUtilsSlice
 */
public class SystemUtilsSlice extends AbilitySlice {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_system_utils_ability_slice);
        initializeViews();
    }

    private void initializeViews() {
        int processorCount = SystemUtils.getProcessorNumbers();
        boolean isRooted = SystemUtils.isRooted();

        Text processorNumber = (Text) findComponentById(ResourceTable.Id_processor_numbers);
        processorNumber.setText(String.valueOf(processorCount));

        Text rootedData = (Text) findComponentById(ResourceTable.Id_is_rooted);
        rootedData.setText(String.valueOf(isRooted));
    }
}
