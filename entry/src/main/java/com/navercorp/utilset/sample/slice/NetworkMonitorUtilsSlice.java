/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.navercorp.utilset.sample.slice;

import com.navercorp.utilset.network.NetworkMonitor;

import com.navercorp.utilset.sample.ResourceTable;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;

import ohos.agp.components.Text;

/**
 * Function description
 * class NetworkMonitorUtilsSlice
 */
public class NetworkMonitorUtilsSlice extends AbilitySlice {
    private static final int PORT_NUMBER = 80;

    private NetworkMonitor networkUtils;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_network_monitor_utils_ability_slice);

        networkUtils = NetworkMonitor.getInstance(this);
        initializeViews();
    }

    private void initializeViews() {
        networkUtils.setWifiDisabled(this);
        networkUtils.setWifiConnectedPreviously(true);
        Text isWifiConnected = (Text) findComponentById(ResourceTable.Id_is_wifi_connected);
        isWifiConnected.setText(String.valueOf(networkUtils.isWifiConnected()));

        Text isWifiEnabled = (Text) findComponentById(ResourceTable.Id_is_wifi_enabled);
        isWifiEnabled.setText(String.valueOf(networkUtils.isWifiEnabled()));

        Text isMobileConnected = (Text) findComponentById(ResourceTable.Id_is_mobile_connected);
        isMobileConnected.setText(String.valueOf(networkUtils.isMobileConnected()));

        Text getMobileState = (Text) findComponentById(ResourceTable.Id_get_mobile_state);
        getMobileState.setText(String.valueOf(networkUtils.getMobileState()));

        Text isNetworkConnected = (Text) findComponentById(ResourceTable.Id_is_network_connected);
        isNetworkConnected.setText(String.valueOf(networkUtils.isNetworkConnected()));

        Text isAirplaneMode = (Text) findComponentById(ResourceTable.Id_is_airplane_mode);
        isAirplaneMode.setText(String.valueOf(networkUtils.isAirplaneModeOn()));

        Text getSimState = (Text) findComponentById(ResourceTable.Id_get_sim_state);
        getSimState.setText(String.valueOf(networkUtils.getSimState()));

        Text getWifiConnectedPreviously = (Text) findComponentById(ResourceTable.Id_get_wifi_connected_previously);
        getWifiConnectedPreviously.setText(String.valueOf(networkUtils.getWifiConnectedPreviously()));

        Text getIpAddressVersionFour = (Text) findComponentById(ResourceTable.Id_get_ip_address_v_four);
        getIpAddressVersionFour.setText(networkUtils.getIpAddress(false));

        Text getIpAddressVersionSix = (Text) findComponentById(ResourceTable.Id_get_ip_address_v_six);
        getIpAddressVersionSix.setText(networkUtils.getIpAddress(true));

        Text getWifiMacAddress = (Text) findComponentById(ResourceTable.Id_get_wifi_mac_address);
        getWifiMacAddress.setText(networkUtils.getWifiMacAddress());

        Text isWifiStpUid = (Text) findComponentById(ResourceTable.Id_is_wifi_stp_uid);
        isWifiStpUid.setText(String.valueOf(networkUtils.isWifiFake(networkUtils.getIpAddress(false), PORT_NUMBER)));
    }

    @Override
    protected void onStop() {
        super.onStop();
        NetworkMonitor.destroy();
    }
}
