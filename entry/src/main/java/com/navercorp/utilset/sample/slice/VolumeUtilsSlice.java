/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.navercorp.utilset.sample.slice;

import com.navercorp.utilset.audio.VolumeUtils;
import com.navercorp.utilset.sample.ResourceTable;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;

import ohos.agp.components.Button;
import ohos.agp.components.Text;

/**
 * Function description
 * class VolumeUtilsSlice
 */
public class VolumeUtilsSlice extends AbilitySlice {
    private Text currentVolume;

    private int volume;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_volume_utils_ability_slice);
        initializeViews();
    }

    private void initializeViews() {
        volume = VolumeUtils.getCurrentVolume();

        currentVolume = (Text) findComponentById(ResourceTable.Id_current_volume);
        currentVolume.setText(String.valueOf(volume));

        Button volumeUp = (Button) findComponentById(ResourceTable.Id_volume_up);
        volumeUp.setClickedListener(component -> {
            VolumeUtils.increaseVolume();
            volume = VolumeUtils.getCurrentVolume();
            currentVolume.setText(String.valueOf(volume));
        });

        Button volumeDown = (Button) findComponentById(ResourceTable.Id_volume_down);
        volumeDown.setClickedListener(component -> {
            VolumeUtils.decreaseVolume();
            volume = VolumeUtils.getCurrentVolume();
            currentVolume.setText(String.valueOf(volume));
        });
    }
}
