/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.navercorp.utilset.sample.slice;

import com.navercorp.utilset.device.DeviceTypeDetector;
import com.navercorp.utilset.device.LauncherTypeDetector;
import com.navercorp.utilset.device.PhoneNumberUtils;

import com.navercorp.utilset.sample.ResourceTable;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;

import ohos.agp.components.Text;

/**
 * Function description
 * class DeviceUtilsSlice
 */
public class DeviceUtilsSlice extends AbilitySlice {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_device_utils_ability_slice);
        initializeViews();
    }

    private void initializeViews() {
        String launcher = String.valueOf(LauncherTypeDetector.getType(getContext()));
        DeviceTypeDetector deviceTypeDetector = new DeviceTypeDetector();
        String device = String.valueOf(deviceTypeDetector.getDeviceType(getContext()));
        PhoneNumberUtils phoneNumberUtils = new PhoneNumberUtils();
        boolean isSmsAvailable = phoneNumberUtils.isAbleToReceiveSms(getContext());

        Text launcherType = (Text) findComponentById(ResourceTable.Id_launcher_type);
        launcherType.setText(launcher);

        Text smsAvailability = (Text) findComponentById(ResourceTable.Id_sms_availability);
        smsAvailability.setText(String.valueOf(isSmsAvailable));

        Text deviceType = (Text) findComponentById(ResourceTable.Id_device_type);
        deviceType.setText(device);
    }
}
