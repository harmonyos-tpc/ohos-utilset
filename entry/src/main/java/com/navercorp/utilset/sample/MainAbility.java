/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.navercorp.utilset.sample;

import com.navercorp.utilset.sample.slice.MainAbilitySlice;
import com.navercorp.utilset.sample.slice.CipherUtilsSlice;
import com.navercorp.utilset.sample.slice.NetworkStateListenerUtilsSlice;
import com.navercorp.utilset.sample.slice.NetworkMonitorUtilsSlice;
import com.navercorp.utilset.sample.slice.VolumeUtilsSlice;
import com.navercorp.utilset.sample.slice.DiskUtilsSlice;
import com.navercorp.utilset.sample.slice.CompressUtilsSlice;
import com.navercorp.utilset.sample.slice.SystemUtilsSlice;
import com.navercorp.utilset.sample.slice.DeviceUtilsSlice;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

/**
 * Function description
 * class MainAbility
 */
public class MainAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(MainAbilitySlice.class.getName());

        addActionRoute("action.CipherUtilsSlice", CipherUtilsSlice.class.getName());
        addActionRoute("action.NetworkStateListenerUtilsSlice", NetworkStateListenerUtilsSlice.class.getName());
        addActionRoute("action.NetworkMonitorUtilsSlice", NetworkMonitorUtilsSlice.class.getName());
        addActionRoute("action.VolumeUtilsSlice", VolumeUtilsSlice.class.getName());
        addActionRoute("action.DiskUtilsSlice", DiskUtilsSlice.class.getName());
        addActionRoute("action.CompressUtilsSlice", CompressUtilsSlice.class.getName());
        addActionRoute("action.SystemUtilsSlice", SystemUtilsSlice.class.getName());
        addActionRoute("action.DeviceUtilsSlice", DeviceUtilsSlice.class.getName());
    }

}
