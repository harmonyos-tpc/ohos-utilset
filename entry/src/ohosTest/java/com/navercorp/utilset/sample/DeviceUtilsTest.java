/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.navercorp.utilset.sample;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import com.navercorp.utilset.device.DeviceTypeDetector;
import com.navercorp.utilset.device.DeviceUtils;
import com.navercorp.utilset.device.LauncherType;

import junitparams.JUnitParamsRunner;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.app.Context;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * Function description
 * class DeviceUtilsTest
 */
@RunWith(JUnitParamsRunner.class)
public class DeviceUtilsTest {
    private Context context = null;

    /**
     * getContextTest
     */
    @Before
    public void getContextTest() {
        context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
    }

    /**
     * getLauncherTypeTest
     */
    @Test
    public void getLauncherTypeTest() {
        assertEquals(LauncherType.OHOS, DeviceUtils.getLauncherType(context));
    }

    /**
     * hasSmsCapabilityTest
     */
    @Test
    public void hasSmsCapabilityTest() {
        assertFalse(DeviceUtils.hasSmsCapability(context));
    }

    /**
     * getDeviceTypeTest
     */
    @Test
    public void getDeviceTypeTest() {
        DeviceTypeDetector deviceTypeDetector = new DeviceTypeDetector();
        String device = String.valueOf(deviceTypeDetector.getDeviceType(context));
        assertEquals("Handset", device);
    }
}
