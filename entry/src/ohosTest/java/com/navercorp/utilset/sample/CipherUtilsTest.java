/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.navercorp.utilset.sample;

import static org.junit.Assert.assertEquals;

import com.navercorp.utilset.cipher.AesCipher;
import com.navercorp.utilset.cipher.CipherUtils;

import org.junit.Test;

/**
 * Function description
 * class CipherUtilsTest
 */
public class CipherUtilsTest {
    /**
     * testCipherUtils
     */
    @Test
    public void testCipherUtils() {
        CipherUtils cipherUtils = new CipherUtils();
        cipherUtils.encrypt("JUnit", "Huawei");
        cipherUtils.decrypt("JUnit", AesCipher.getsEncryptedText());
        String decryptedText = AesCipher.getsDecryptedText();
        assertEquals("Huawei", decryptedText);
    }
}
