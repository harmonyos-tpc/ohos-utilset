/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.navercorp.utilset;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import com.navercorp.utilset.storage.DiskUtils;

import junitparams.JUnitParamsRunner;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.app.Context;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * Function description
 * class DiskUtilsTest
 */
@RunWith(JUnitParamsRunner.class)
public class DiskUtilsTest {
    private Context context = null;

    /**
     * getContextTest
     */
    @Before
    public void getContextTest() {
        context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
    }

    /**
     * isExternalStorageMountedTest
     */
    @Test
    public void isExternalStorageMountedTest() {
        assertTrue(DiskUtils.isExternalStorageMounted());
    }

    /**
     * getExternalDirPathTest
     */
    @Test
    public void getExternalDirPathTest() {
        assertEquals("/storage/emulated/0/Android/data/com.navercorp.utilset.sample/cache",
                DiskUtils.getExternalDirPath(context));
    }

    /**
     * getExternalContextRootDirTest
     */
    @Test
    public void getExternalContextRootDirTest() {
        assertEquals("/storage/emulated/0/Android/data/com.navercorp.utilset.sample/files",
                DiskUtils.getExternalContextRootDir(context));
    }
}
